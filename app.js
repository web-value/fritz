"use strict"
//requires
var http = require('http')
  , express = require('express')
  , path = require('path')
  , logger = require('./lib/logger.js')
  , mongoose = require('mongoose')
  , config = require('./app/config/config.js')
  , app = new express();


//initialize utils
require('./lib/utils.js');

//initialize app
app.set('port',process.env.PORT || config.server.port); 
app.set('views',path.join(__dirname , config.views.path));
app.set('view engine', config.views.engine);

//setup static 
app.use(express.static(path.join(__dirname , config.static.path)));

//setup routes
require('./lib/boot.js')(app);

//initialize database
mongoose.connect('mongodb://'+config.db.host+':'+(config.db.port||27017)+'/'+config.db.name);
var db = mongoose.connection;
db.on('error', function callback () {
  logger.error('database connection error: ');
});

db.once('open', function callback () {
	 logger.info('database ('+config.db.name+') is ready');
	 //console.log(db);
	//initialize http
	http.createServer(app).listen(app.get('port'),function(){
		logger.info('Express server listening on port '+app.get('port'));
	});

	//run test
	if(config.environment == 'test'){
	  	require('./tests/test.js');
	}


});

