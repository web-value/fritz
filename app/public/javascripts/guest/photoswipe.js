
function initPhotoSwipeFromDOM(gallerySelector) {
    
    var parseThumbnailElements = function (el) {
        var thumbElements = $(el).children(':not([aria-hidden="true"])').get(),
            numNodes = thumbElements.length,
            items = [],
            figureEl,
            linkEl,
            size,
            item,
            caption;
        for (var i = 0; i < numNodes; i++) {

            figureEl = thumbElements[i];

            if (figureEl.nodeType !== 1) {
                continue;
            }

            linkEl = figureEl.getElementsByClassName("linked")[0]; // <a> element
            size = linkEl.getAttribute('data-size').split('x');
            caption = linkEl.getAttribute('alt');

            item = {
                src: linkEl.getAttribute('href'),
                w: parseInt(size[0], 10),
                h: parseInt(size[1], 10),
                title: caption
            };

            if (figureEl.children.length > 1) {
                item.title = figureEl.children[1].innerHTML;
            }

            if (linkEl.children.length > 0) {
                item.msrc = linkEl.children[0].getAttribute('src');
            }

            item.el = figureEl;
            items.push(item);
        }

        return items;
    };

    var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
    };

    var onThumbnailsClick = function (e) {
        e = e || window.event;

        var eTarget = e.target || e.srcElement;
        if(eTarget.tagName.toLowerCase() != 'img')
        	return true;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var clickedListItem = closest(eTarget, function (el) {
            return (el.tagName && el.tagName.toUpperCase() === 'IMG');
        });

        if (!clickedListItem) {
            return;
        }

        var $clickedListItem = $(clickedListItem)
        		clickedGallery = $clickedListItem.closest('.gridgallery'),
            childNodes = $(clickedGallery).children(':not([aria-hidden="true"])').get(),
            numChildNodes = childNodes.length,
            nodeIndex = 0,
            index = 0;
        for (var i = 0; i < numChildNodes; i++) {

            if (childNodes[i].getElementsByTagName("img")[0].nodeType !== 1) {
                continue;
            }

            if (childNodes[i].getElementsByTagName("img")[0] === clickedListItem) {
                index = nodeIndex;
                break;
            }
            nodeIndex++;
        }

        if (index >= 0) {
            openPhotoSwipe(index, clickedGallery, false);
        }
        return false;
    };

    var openPhotoSwipe = function (index, galleryElement, disableAnimation) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
            gallery,
            options,
            items;
        items = parseThumbnailElements(galleryElement);
        var $galleryElement = $(galleryElement);
        options = {
            index: index,
            history: false,
            loop: false,
            closeOnScroll: false,
            galleryUID: $galleryElement.data('pswp-uid'),
            getThumbBoundsFn: function (index) {
                var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                    pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                    rect = thumbnail.getBoundingClientRect();
                console.log(items[index].el.getElementsByTagName('h2')[0].innerHTML);
                return {
                    x: rect.left,
                    y: rect.top + pageYScroll,
                    w: rect.width,
                };
            }

        };

        if (disableAnimation) {
            options.showAnimationDuration = 0;
        }

        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    };

    var galleryElements = document.querySelectorAll(gallerySelector);

    for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
    }
};
