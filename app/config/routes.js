var __basePath = process.cwd()
	, logger = require(__basePath+'/lib/logger')
	, path = require('path')
  , config = require('./config.js')
	, guestController = require(path.join(config.__basePath , config.controllers.path, 'guest'));

module.exports = function(app , setting)
{
	//add your custome routes here
	app.all('/', guestController['index']);
}

