const evn = 'production';

var configParser = require('yaml-config')
  , config = configParser.readConfig('./app/config/app.yaml',evn)
  , dic = {}
  , tags = {};


config.__basePath = process.cwd();
config.environment = evn;

config.set = function(key , value , tag){
	dic[key] = value;
	if(tag) config.assignTag(key , tag);
	return config;
}

config.assignTag = function(key , tag)
{
	if(!(tag in tags)) tags[tag] = [];
	tags[tag].push(key);
}

config.get = function(key)
{
	return dic[key];
}

config.getKeysByTag = function(tag)
{
	return tags[tag];
}

config.getValuesByTag = function(tag)
{
	var keys = config.getKeysByTag(tag);
	return keys?keys.map(function(key){return config.get(key)}):null;
}

module.exports = config;