var path = require('path')
  , http = require("http")
  , fs = require("fs")
  , extract = require('extract-zip')
  , mkdirp = require('mkdirp')
  , config = require('../../config/config.js')
  , logger = require(path.join(config.__basePath, '/lib/logger'))
  , CategoryModel = require(path.join(config.__basePath, config.models.path, 'category.js'));

exports.index = function(req,res,next){
	CategoryModel.find({}, function(err, categories){
		 res.render("guest/index", {categories: categories});
	});
	
}