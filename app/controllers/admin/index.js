var path = require('path')
  , http = require("http")
  , fs = require("fs")
  , extract = require('extract-zip')
  , mkdirp = require('mkdirp')
  , config = require('../../config/config.js')
  , logger = require(path.join(config.__basePath, '/lib/logger'))
  , imageSize = require('image-size')
  , CategoryModel = require(path.join(config.__basePath, config.models.path, 'category.js'))
  , Model = require(path.join(config.__basePath, config.models.path, 'fetch/request.js'))
  , fileManager = require(path.join(config.__basePath, config.services.path, 'file-manager.js'));


exports.index = function(req,res,next){
	var api_key = 'invalid';
	if(config.api.key && /^\w{4}(-\w{4}){6}$/.test(config.api.key))
		api_key = config.api.key.replace(/(?!^\w{4})(\w{4})(?!\w*$)/g, '****');
	res.render("admin/index", {api_key: api_key});
}


var parseFetchedData = function(callback){
	fileManager.getPath(config.api.responses.path, 'issues.json', function(err, issuesPath){
		if(err) callback(err);
		else{
			fileManager.getDir(config.assets.path, function(err, dir){
				if(err) callback(err);
				else{
					fileManager.getPath(config.api.responses.path, 'issues.zip', function(err, storagePath){
						if(err) callback(err);
						else{
							fileManager.removeAllFileInDir(dir);
							extract(storagePath, {dir: dir}, function(err, result){
								//this code not good enough for keep archive
								var contents = fs.readFileSync(issuesPath);
				 				var jsonContent = JSON.parse(contents);
				 				var categories = {};
				 				var defaultImageInfo = getFileInfo('image', config.assets.default.image.path, config.assets.default.image.url);
				 				
				 				jsonContent.issues.forEach( function(issue) { 
				 					var category_id = issue.source.category.id;
								  if(!(category_id in categories)){
								  	categories[category_id] = issue.source.category;
								  	categories[category_id]['sources'] = [];
								  }

								  var source = issue.source;
									delete source['category'];
									var issueObject = {'date': issue['date'], 'available': issue['available'], 'files': null};
									
									issueObject['files'] = {};
									var fileName = 'jaaar.com_'+source['slug'].capitalizeFirstLetter()+'_'+issue['date']['georgian'];
									var filePath = path.join(dir, fileName);
									[{'type':'image', 'extension':'jpg', 'default_info': defaultImageInfo}, {'type':'pdf', 'extension':'pdf', 'default_info': null}].forEach(function(file){
										var info = getFileInfo(file['type'], filePath+'.'+file['extension'], path.join(config.assets.url, fileName+'.'+file['extension']));
										if(!info)
											info = file['default_info'];
										issueObject['files'][file['type']] = info;
									});
										
									source['issues'] = [issueObject];
								  categories[category_id]['sources'].push(source);

								});

								CategoryModel.replaceCategories(categories, function(erros, res){
									//handle error
									callback(null, jsonContent);
								})
							});
						}
					});
				}
			});
		}
	});
}

var getFileInfo = function(fileType, filePath, fileUrl){
	var info = null;
	try{
			var stat = fs.statSync(filePath);
			if(stat){
				info = {path: fileUrl};
				if(fileType.toLowerCase() == 'image'){
					var dimensions = imageSize(filePath);
					info['meta'] = {width: dimensions.width, height: dimensions.height};
				}
			}
		}catch(err){
			logger.error(err);
		}
	return info;
}

var fetchDataFromJaaar = function(requests, next){
	var request = requests.shift();
	Model.addLogForSpecificRequestById(request.id, {'message': 'call... ---> method: GET | host: '+config.api.hostname+' | url:  '+request.url, 'status': 'Waiting'}, function(err, result) {
  	if(err) return next(err);
		else if(result) {
			fetchDataFromServer(request, function(response, err){
				if(err) {
					Model.updateStatusWithLogForSpecificRequestById(request.id, 'Failed', {'message': err.message, 'details': err, 'status': 'Fail'}, function(err, result){
						if(err) return next(err);
					});
					return next(err);
				}
				else if(response) {
					var statusCode = parseInt(response.statusCode, 10);
					if(statusCode < 400){
						// log success response
						Model.addLogForSpecificRequestById(request.id, {'message': 'Recive Success Response For '+request.url+' | status_code #'+statusCode, 'status': 'Success'}, function(err, result) {
							if(err) return next(err);
							else if(result){
								//log start fetching data
								Model.addLogForSpecificRequestById(request.id, {'message': 'Fetching Data ...', 'status': 'Waiting'}, function(err, result) {
									if(err) return next(err);
									else if(result){
										//download body
										getBodyFromResponse(response, function(err, body){
											//TODO handle error
											//write response in file
											fileManager.saveBufferInFile(config.api.responses.path, request.response_name, body, function(err, result){
												if(err) {
													//log request failed
													Model.updateStatusWithLogForSpecificRequestById(request.id, 'Failed', {'message': err.message, 'details': err, 'status': 'Fail'}, function(err, result){
														if(err) return next(err);
													});
													return next(err);
												}
												else{
													//log response saved
													Model.addLogForSpecificRequestById(request.id, {'message': 'Response saved', 'details': {'link': config.api.responses.url+'/'+request.response_name}, 'status': 'Success'}, function(err, result){
														if(err) return next(err);
														else{
															//call request callback
															request.callback(body, function(err, result){
																if(err) {
																	Model.updateStatusWithLogForSpecificRequestById(request.id, 'Failed', {'message': err.message, 'details': err, 'status': 'Fail'}, function(err, result){
																		if(err) return next(err);
																	});
																}else{
																	//check for next request in queue
																	if(requests.length) fetchDataFromJaaar(requests, next);
																	else {
																		//log for start parse fetched data
																		Model.addLogForSpecificRequestById(request.id, {'message': 'Parse Fetched Data ...', 'status': 'Waiting'}, function(err, result) {
																			if(err) return next(err);
																			else {
																				//start parse fetched data
																				parseFetchedData(function(err, res){
																					if(err) next(err);
																					else{
																						Model.updateStatusWithLogForSpecificRequestById(request.id, 'Completed', {'message': 'Completed', 'status': 'Success'}, function(err, result) {
																							if(err) return next(err);
																						});
																					}
																				});// end of parse fetched data
																			}
																		});// end of log for parse fetched data
																	}
																}
															});// end of request callback
														}
													});//end of response save log
												}
											});//end of save buffer in file
										});//end of get body
									}
									else return next({error: "Internal Server Error"});		
								});//end of log fetching data
							}
							else return next({error: "Internal Server Error"});		
						});//end of log success response
					}else{
						// log fail request
						getMessageForFailRequest(response, function(err, message){
							//TODO handle error
							Model.updateStatusWithLogForSpecificRequestById(request.id, 'Failed', {'message': message +' | code #'+statusCode, 'status': 'Fail'}, function(err, result){
								if(err) return next(err);
							});
						});
					}
				}
				else return next({error:'Internal Server Error'});
			});//end of fetch data from server
		}
		else return next({error:'Internal Server Error'});
  });//end of log call api
}


var getBodyFromResponse = function(response, callback){
	var chunks = [];
	var body = "";
	response.on("data", function (chunk) {
		chunks.push(chunk);
	});

  response.on("end", function () {
  	body = Buffer.concat(chunks);
  	callback(null, body);
  });

  /*response.on("error", function (err) {
  	callback(err);
  });*/
}

var getMessageForFailRequest = function(response, callback){
	var message = 'Error';
	var statusCode = parseInt(response.statusCode, 10);
	if(statusCode == 404) callback(null, 'Url Not Found');
	else if(statusCode >= 500) callback(null, 'Server Error');
	else{
		getBodyFromResponse(response, function(err, body){
			try{
				var json = JSON.parse(body);
				if('message' in json) message = json.message;
				else message = body;
			}catch(err){
				message = body;
			}			

			callback(null, message);
		});
		
	}
}

var fetchDataFromServer = function(request, callback){
		http.request({
		  "method": "GET",
		  "path": '/'+request.url,
		  "hostname": config.api.hostname,
		  "headers": {
		  	'User-Agent': config.name,//temp
		    "jaaar-api-key": config.api.key,
		    "accept": "application/json"
		  }
		}, callback).on('error',function(err){
		   logger.error("Error: " + err.message); 
		   console.log( err.stack );
		   callback(null, err);
		}).end();
}


exports.get_issues = function(req, res, next)
{
	Model.addRequest({status: 'Running'},  function(err, result) {
  	if(err) return next(err);
		else if(result) {
			if('insertedIds' in result && result.insertedIds.length > 0){
				var id = result.insertedIds[0];
				var url = 'packages/v1';
				fetchDataFromJaaar([{
															id: id,
															url: url+"/issues",
															response_name: 'issues.json',
															callback: function(res, callback){
																callback(null, res);
															}
														},{
															id: id,
															url: url+"/files?include_type=all",
															response_name: 'issues.zip',
															callback: function(res, callback){
																callback(null, res);
															}
														}], next);
						
				return res.json({request_id: id});
			}
			return next({error:'Internal Server Error'});
		}
		else return next({error:'Internal Server Error'});
  });
	
}

exports.get_request = function(req, res, next)
{
	Model.findById(req.query.request_id, function(err, doc) {
        if(err) return next(err);
        else if(doc) {
        	return res.json(doc);
        }
		    else return res.notFound().send({error: 'Not Found'});
   });
}
