//require
var __basePath = process.cwd()
  , mongoose = require('mongoose')
  , db = mongoose.connection
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , logger = require(__basePath+'/lib/logger');


//define Schema
var getObjectSchema = function(){
  return { created_at: { type: Date, default: Date.now, required: true }
          , date: { georgian: {type: String, required: true, trim: true  }, jalali: {type: String, required: true, trim: true  }}
          , available: {type: Boolean, default: 0, required: true}
          , files: { image: { path: {type: String, required: false, trim: true }
                            , meta: {width:{type:Number, default:0, required:true}
                                   , height:{type:Number, default:0, required:true}}}
                   , pdf: { path: {type: String, required: false, trim: true }}}
        }
}

//create schema
var schema = mongoose.Schema(getObjectSchema());


//methods 
schema.statics.getSchema = function(){
    return schema;
}

//assign Model
var Issue = db.model('issue', schema);

module.exports = Issue;