//require
var __basePath = process.cwd()
  , mongoose = require('mongoose')
  , db = mongoose.connection
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , logger = require(__basePath+'/lib/logger')
  , Source = require('./source');


//define Schema
var getObjectSchema = function(){
  return { id: { type: String, required: true, index: { unique: true, dropDups: true }, trim: true  }
          , title: { type: String, required: true, trim: true }
          , slug: { type: String, required: true, trim: true  }
          , sources: [Source.getSchema()]
        }
}

//create schema
var schema = mongoose.Schema(getObjectSchema()).index({ id: 1});


//methods 
schema.statics.getSchema = function(){
    return schema;
}

schema.statics.replaceCategories = function(categories, callback){
  Category.remove({}, function(err) { 
    if(err) callbacl(err);
    else{
      var arrayOfCategories = [];
      var erros = null;
      var resules = null;
      var recurciveAddCategories = function(categories, callback){
        if(categories.length > 0){
          var category = categories.pop();
          Category.collection.insert(category, function(err, res){
            if(err) {
              if(!erros) erros = [];
              erros.push(err);
            }
            else {
              if(!resules) resules = [];
              resules.push(res);
              recurciveAddCategories(categories, callback);
            }
          });
        }else{
          callback(erros, resules)
        }
      };

      for (var key in categories) { 
        if (categories.hasOwnProperty(key)) {
          arrayOfCategories.push(categories[key]);          
        }
      }

      recurciveAddCategories(arrayOfCategories.reverse(), callback);

    }
  });
}

//assign Model
var Category = db.model('category', schema);

module.exports = Category;