//require
var __basePath = process.cwd()
  , mongoose = require('mongoose')
  , db = mongoose.connection
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , logger = require(__basePath+'/lib/logger')
  , Issue = require('./issue');


//define Schema
var getObjectSchema = function(){
  return { id: { type: String, required: true, index: { unique: true, dropDups: true }, trim: true  }
          , title: { type: String, required: true, trim: true }
          , slug: { type: String, required: true, trim: true  }
          , website: { type: String, required: false, trim: true  }
          , issues: [Issue.getSchema()]
        }
}

//create schema
var schema = mongoose.Schema(getObjectSchema()).index({ id: 1});


//methods 
schema.statics.getSchema = function(){
    return schema;
}

//assign Model
var Source = db.model('source', schema);

module.exports = Source;