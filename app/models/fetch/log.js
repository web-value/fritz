//require
var __basePath = process.cwd()
  , mongoose = require('mongoose')
  , db = mongoose.connection
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , logger = require(__basePath+'/lib/logger');


//define Schema
var getObjectSchema = function(){
  return { created_at: { type: Date, default: Date.now, required: true }
          , status: { type: String, enum: ['Success','Waiting','Fail'], required: true, trim: true }
          , message: { type: String, required: true, trim: true  }
          , details: Object
        }
}

//create schema
var schema = mongoose.Schema(getObjectSchema(), {
  toObject: {virtuals: true}, toJSON: {virtuals: true}
}).index({ created_at: 1});


//virtual fields
schema.virtual('timestamp_ms').get(function() {
  return this.created_at.getTime();
});

//methods 
schema.statics.getSchema = function(){
    return schema;
}

//assign Model
var Log = db.model('FetchLog', schema);

module.exports = Log;