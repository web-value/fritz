//require
var __basePath = process.cwd()
  , mongoose = require('mongoose')
  , db = mongoose.connection
  , logger = require(__basePath+'/lib/logger')
  , Schema = mongoose.Schema
  , ObjectId = Schema.ObjectId
  , Log = require('./log');

//define Schema
var getObjectSchema = function(){
  return { created_at: { type: Date, default: Date.now, required: true }
          , logs: [Log.getSchema()]
          , status: { type: String, enum: ['Queued', 'Running', 'Cancelled', 'Failed', 'Completed'], required: true, trim: true }
        }
}


//create schema
var schema = mongoose.Schema(getObjectSchema(), {
  toObject: {virtuals: true}, toJSON: {virtuals: true}
}).index({ created_at: 1});


//virtual fields
schema.virtual('created_at_ms').get(function() {
  return this.created_at.getTime();
});

//methods 
schema.statics.getSchema = function(){
    return schema;
}

schema.statics.addRequest = function(data, callback){
  Request.collection.insert(data, callback)
}

schema.statics.updateStatusWithLogForSpecificRequestById= function(request_id, status, log, callback){
  Request.findById(request_id, function (err, request) {
    if(err) callback(err, null);
    else{
      request.status = status;
      request.logs.push(log);
      request.save(callback);
    }    
  });
}

schema.statics.updateStatusForSpecificRequestById = function(request_id, status, callback){
  Request.findById(request_id, function (err, request) {
    if(err) callback(err, null);
    else{
      request.status = status;
      request.save(callback);
    }    
  });
}

schema.statics.addLogForSpecificRequestById = function(request_id, log, callback){
  Request.findById(request_id, function (err, request) {
    if(err) callback(err, null);
    else{
      request.logs.push(log);
      request.save(callback);
    }    
  });
}

//assign Model
var Request = db.model('FetchRequest', schema);

module.exports = Request;