var path = require('path')
  , fs = require("fs")
  , extract = require('extract-zip')
  , mkdirp = require('mkdirp')
  , config = require('../config/config.js')
  , logger = require(path.join(config.__basePath, '/lib/logger'));
 

var removeAllFileInDir = function(dirPath, recursively) {
  if(recursively === undefined) recursively = false;
  try { var files = fs.readdirSync(dirPath); }
  catch(e) { return; }
  var fileLength = files.length;
  if (fileLength> 0){
  	for (var i = 0; i < fileLength ; i++) {
      var filePath = path.join(dirPath, files[i]);
      if (fs.statSync(filePath).isFile()) fs.unlinkSync(filePath);
      else if(recursively) emptyDir(filePath);
    }
  } 
};

var saveBufferInFile = function(dir, fileName, buffer, callback){
	getDir(dir, function(err, dir){
		if(err) callback(err);
		else _saveBuffer(path.join(dir, fileName), buffer, callback);
	});
};

var getDir = function(dirPath, callback){
	var dir = path.join(config.__basePath, dirPath)
	mkdirp(dir, function(err){ callback(err, dir); });
};

var getPath = function(dir, fileName, callback){
	getDir(dir, function(err, dir){
		if(err) callback(err);
		else callback(null, path.join(dir, fileName));
	});
}

var _saveBuffer = function(filePath, buffer, callback){
	fs.open(filePath, 'w', function(err, fd) {
    if (err) callback(err);
    else{
    	fs.write(fd, buffer, 0, buffer.length, null, function(err) {
        if (err) callback(err);
        fs.close(fd, callback)
    	});
    }
	});
};

exports.getDir = getDir;
exports.saveBufferInFile = saveBufferInFile;
exports.removeAllFileInDir = removeAllFileInDir;
exports.getPath = getPath;


