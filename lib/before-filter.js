var config = require('./../app/config/config.js')
  , path = require('path')
  , url = require('url')
  , logger = require('./logger');

module.exports = function(req, res, next){
    //Nothing yet

	if(config.environment == 'test'){
		//logger.info('Call before-filter.js');
	}  
  next();
}