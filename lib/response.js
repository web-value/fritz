// Informational 1xx
const HTTP_CONTINUE         = 100;
const SWITCH_PROTOCOLS      = 101;

// Successful 2xx
const OK                    = 200;
const CREATED               = 201;
const ACCEPTED              = 202;
const NONAUTHORITATIVE      = 203;
const NO_CONTENT            = 204;
const RESET_CONTENT         = 205;
const PARTIAL_CONTENT       = 206;

// Redirection 3xx
const MULTIPLE_CHOICES      = 300;
const MOVED_PERMANENTLY     = 301;
const FOUND                 = 302;
const SEE_OTHER             = 303;
const NOT_MODIFIED          = 304;
const USE_PROXY             = 305;
// 306 is deprecated but reserved
const TEMP_REDIRECT         = 307;

// Client Error 4xx
const BAD_REQUEST           = 400;
const UNAUTHORIZED          = 401;
const PAYMENT_REQUIRED      = 402;
const FORBIDDEN             = 403;
const NOT_FOUND             = 404;
const NOT_ALLOWED           = 405;
const NOT_ACCEPTABLE        = 406;
const PROXY_AUTH_REQUIRED   = 407;
const REQUEST_TIMEOUT       = 408;
const CONFLICT              = 409;
const GONE                  = 410;
const LENGTH_REQUIRED       = 411;
const PRECONDITION_FAILED   = 412;
const LARGE_REQUEST_ENTITY  = 413;
const LONG_REQUEST_URI      = 414;
const UNSUPPORTED_TYPE      = 415;
const UNSATISFIABLE_RANGE   = 416;
const EXPECTATION_FAILED    = 417;

// Server Error 5xx
const SERVER_ERROR          = 500;
const NOT_IMPLEMENTED       = 501;
const BAD_GATEWAY           = 502;
const UNAVAILABLE           = 503;
const GATEWAY_TIMEOUT       = 504;
const UNSUPPORTED_VERSION   = 505;
const BANDWIDTH_EXCEEDED    = 509;

var express = require("express")
  , http = require("http")
  , response = http.ServerResponse.prototype;


// Informational 1xx
response.httpContinue = function(){
	this.status(HTTP_CONTINUE);
	return this;
}

response.switchProtocols = function(){
	this.status(SWITCH_PROTOCOLS);
	return this;
}

// Successful 2xx

response.ok = function(){
	this.status(OK);
	return this;
}

response.created = function(){
	this.status(CREATED);
	return this;
}


response.accepted = function(){
	this.status(ACCEPTED);
	return this;
}

response.nonAuthoritative = function(){
	this.status(NONAUTHORITATIVE);
	return this;
}


response.noContent = function(){
	this.status(NO_CONTENT);
	return this;
}

response.resetContent = function(){
	this.status(RESET_CONTENT);
	return this;
}

response.partialContent = function(){
	this.status(PARTIAL_CONTENT);
	return this;
}

response.multipleChoices = function(){
	this.status(MULTIPLE_CHOICES);
	return this;
}

response.movedPermanently = function(){
	this.status(MOVED_PERMANENTLY);
	return this;
}

response.found = function(){
	this.status(FOUND);
	return this;
}

response.seeOther = function(){
	this.status(SEE_OTHER);
	return this;
}

response.notModified = function(){
	this.status(NOT_MODIFIED);
	return this;
}

response.useProxy = function(){
	this.status(USE_PROXY);
	return this;
}

response.tempRedirect = function(){
	this.status(TEMP_REDIRECT);
	return this;
}

// Client Error 4xx
response.badRequest = function(){
	this.status(BAD_REQUEST);
	return this;
}

response.unauthorized = function(){
	this.status(UNAUTHORIZED);
	return this;
}

response.paymentRequired = function(){
	this.status(PAYMENT_REQUIRED);
	return this;
}

response.forbidden = function(){
	this.status(FORBIDDEN);
	return this;
}

response.notFound = function(){
	this.status(NOT_FOUND);
	return this;
}

response.notAllowed = function(){
	this.status(NOT_ALLOWED);
	return this;
}

response.notAcceptable = function(){
	this.status(NOT_ACCEPTABLE);
	return this;
}

response.proxyAuthRequired = function(){
	this.status(PROXY_AUTH_REQUIRED);
	return this;
}

response.requestTimeout = function(){
	this.status(REQUEST_TIMEOUT);
	return this;
}

response.conflict = function(){
	this.status(CONFLICT);
	return this;
}
    
response.gone = function(){
	this.status(GONE);
	return this;
}

response.lengthRequired = function(){
	this.status(LENGTH_REQUIRED);
	return this;
}
    
response.preconditionFailed = function(){
	this.status(PRECONDITION_FAILED);
	return this;
}

response.largeRequestEntity = function(){
	this.status(LARGE_REQUEST_ENTITY);
	return this;
}
  
response.longRequestUri = function(){
	this.status(LONG_REQUEST_URI);
	return this;
}
    
response.unsupportedType = function(){
	this.status(UNSUPPORTED_TYPE);
	return this;
}

response.unsatisfiableRange = function(){
	this.status(UNSATISFIABLE_RANGE);
	return this;
}
 
response.expectationFailed = function(){
	this.status(EXPECTATION_FAILED);
	return this;
}
    
// Server Error 5xx
response.serverError = function(){
	this.status(SERVER_ERROR);
	return this;
}

response.notImplemented = function(){
	this.status(NOT_IMPLEMENTED);
	return this;
}
     
response.badGateway = function(){
	this.status(BAD_GATEWAY);
	return this;
}

response.unavailable = function(){
	this.status(UNAVAILABLE);
	return this;
}  

response.gatewayTimeout = function(){
	this.status(GATEWAY_TIMEOUT);
	return this;
}

response.unsupportedVersion = function(){
	this.status(UNSUPPORTED_VERSION);
	return this;
}
     
response.bandwidthExceeded = function(){
	this.status(BANDWIDTH_EXCEEDED);
	return this;
}