var winston = require('winston')
  ,	path = require('path')
  , config = require('./../app/config/config.js')
  , logConfig = config.log
  , loggerConfig = {transports:[],exceptionHandlers:[]};


//check active options for logger
var isActive = function(type , scope){
   return !!((type in logConfig)&&(scope in logConfig[type])&&logConfig[type][scope]);
}

//initialize logger config
for (var key in loggerConfig)
{

    if(isActive(key , 'console'))
      loggerConfig[key].push(new (winston.transports.Console)({ json: false, timestamp: true }));

    if(isActive(key , 'path'))
      loggerConfig[key].push(new winston.transports.File({ filename: path.join(config.__basePath , logConfig[key].path), json: false }));
}

//initialize logger
var logger = new (winston.Logger)(loggerConfig);


/**
*  little hack for winston to show file name in log message
*  refrance : http://stackoverflow.com/questions/13410754/i-want-to-display-the-file-name-in-the-log-statement
*/

['info','error'].forEach(function(fnName){
    var old = logger[fnName];
    logger[fnName] = function() {
      var fileAndLine = traceCaller(1);
      arguments[0] = fileAndLine + ":" + arguments[0];
      return old.apply(this, arguments);
    }
});

/**
* examines the call stack and returns a string indicating 
* the file and line number of the n'th previous ancestor call.
* this works in chrome, and should work in nodejs as well.  
*
* @param n : int (default: n=1) - the number of calls to trace up the
*   stack from the current call.  `n=0` gives you your current file/line.
*  `n=1` gives the file/line that called you.
*/
function traceCaller(n) {
  if( isNaN(n) || n<0) n=1;
  n+=1;
  var s = (new Error()).stack
    , a=s.indexOf('\n',5);
  while(n--) {
    a=s.indexOf('\n',a+1);
    if( a<0 ) { a=s.lastIndexOf('\n',s.length); break;}
  }
  b=s.indexOf('\n',a+1); if( b<0 ) b=s.length;
  a=Math.max(s.lastIndexOf(' ',b), s.lastIndexOf('/',b));
  b=s.lastIndexOf(':',b);
  s=s.substring(a+1,b);
  return s;
}
  
module.exports = logger;