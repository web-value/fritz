/**
* Idea from https://github.com/visionmedia/express/blob/master/examples/mvc/lib/boot.js
*/

var fs = require('fs')
  ,	path = require('path')
  , bodyParser = require('body-parser')
  , config = require('../app/config/config.js')
  , methodOverride = require('method-override')
  , logger = require('./logger.js')
  , response = require('./response.js');

const BEFORE = 'before';


var getPath = function (module , controller , action)
{
	if(!module)
		module = '/';
	return path.join(module,controller,action);
}


var getControllerInfo = function(dir , base) {
	var results = [];
	if(!base) base = [];

    var list = fs.readdirSync(dir)
    list.forEach(function(fileName) {
        file = path.join(dir,fileName);
        var stat = fs.statSync(file)
        if (stat && stat.isDirectory()) results = results.concat(getControllerInfo(file , base.concat(fileName)));
        else {
        	var routeInfo = getRouteObject(dir , fileName , base);
        	if(routeInfo) results.push(routeInfo);
        }
    })

    return results;
}



var getFileInfo = function(file)
{
	const reg =  /(.*)\.(.*)$/g;
	if(fileInfo = reg.exec(file))
		return {fileName: fileInfo[1], fileExtension: fileInfo[2].toLowerCase()};
	return null;
}


var getRouteInfo = function(fileName , base){
	var controllerName = fileName;
	var prefix = '/';
	var keys = base || [];
	if(base && base.length)
	{
		var prefixs = [];
		var lastDir = '';
		base.forEach(function(dir){
			if(lastDir)
				prefixs.push(':'+lastDir.toLowerCase()+'Id');
			prefixs.push(dir);
			lastDir = dir;
		})

		if(fileName.toLowerCase() === 'index')
		{
			controllerName = prefixs.pop();
		}
		
		prefix = '/'+prefixs.join('/');
	}else{
		keys = [fileName];
	}

	return {prefix: prefix, controllerName: controllerName, controllerKey: keys.join('_')};
}


var getRouteObject = function(basePath , file , base){
	var fileInfo = getFileInfo(file);
	if(fileInfo && fileInfo.fileExtension === 'js')
	{
		var routeInfo = getRouteInfo(fileInfo.fileName , base);
		routeInfo.filePath = path.join(basePath,file);
		return routeInfo;
	}
	return routeInfo;
}


var route = function(app , routeInfo)
{
	var obj = require(routeInfo.filePath)
	  , controllerName = routeInfo.controllerName
	  , prefix = routeInfo.prefix
	  , _method = _path = '';

	// before middleware support
	if (BEFORE in obj) {
		var _path = getPath(prefix ,controllerName,'');
		app.all(_path, obj[BEFORE]);
		logger.info(' ALL %s -> before', _path);
	}


	// generate routes based
	// on the exported methods
	for (var key in obj) {
		// "reserved" exports
		if(key === 'config'){
				config.set(routeInfo.controllerKey, obj[key]);
		}
		else if (!~['name', 'prefix', BEFORE].indexOf(key)){
    //set route
			var _path = getPath(prefix , controllerName , key);
			var _paths = [_path];
			if(key == 'index') _paths.push(getPath(prefix , controllerName , ''));
			_paths.forEach(function(__path){
				app.all(__path, obj[key]);
	  		//log route
		  	logger.info('%s -> %s', __path, key);
			});
	  }
	}
}

module.exports = function(app){

	//body parser
	//app.use(bodyParser());
	// parse application/x-www-form-urlencoded
	app.use(bodyParser.urlencoded({ extended: false }))

	// parse application/json
	app.use(bodyParser.json())

	//method override
	app.use(methodOverride());

	app.use(require('./acl')); 
	app.use(require('./before-filter')); 
	
	// routeing
	var controllersInfo = getControllerInfo(path.join(config.__basePath , config.controllers.path));
	controllersInfo.forEach(function(controllerInfo){
		route(app,controllerInfo);
	})


	//custome routes
	require(path.join(config.__basePath,'app/config/routes.js'))(app,config);

	//logErrors
	app.use(function(err, req, res, next) {
	  logger.error(err.stack);
	  next(err);
	});


	//clientErrorHandler
	app.use(function(err, req, res, next) {
		if (err) {
		  switch (err.name) {
		    case 'ValidationError':
		      for (field in err.errors) {
		        switch (err.errors[field].type) {
		          case 'exists':
		            	res.confilict();
		            break;
		          default:
		          case 'invalid':
		            	res.badRequest();
		            break;
		        }
		      }
		      break;
		    default:
		      return next(err);
		  }
		  return res.send(err);
		}
		else if (req.xhr) {
			return res.serverError().send({ error: 'Something blew up!' });
		} else {
			return next(err);
		}
	});

	//errorHandler
	app.use(function(err, req, res, next) {
	  //res.serverError().render('_5xx', { error: err });
	});

	//notfoundHandler
	app.use(function(req , res){
	  res.notFound().render('_404' , {url : req.url})
	});
}